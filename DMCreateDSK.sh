#!/bin/bash

# Build the exported build_sdk
bitbake -c populate_sdk $IMAGE

# Archive the results
cp tmp*/deploy/sdk/* $ARCHIVE
